<footer id="footer"><!--Footer-->

    <div class="row">
        <div class="footer">
            <div class="container">
                <p class="pull-left">Copyright © 2020 E-SHOPPER. All rights reserved.</p>
                <p class="pull-right">Built by <span><a target="_blank"
                                                        href="http://www.themeum.com">Alexander Gotfrid</a></span></p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->


<!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}
{{--<script src="{{asset('js/jquery-3.5.1.min.js')}}"></script>--}}
<script src="{{asset('js/jquery.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('js/price-range.js')}}"></script>
<script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>
