@extends('layouts.index')
@section('center')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
{{--            @if(Auth::check())--}}
                <div class="shopper-informations">
                    <div class="row">
                        <div class="col-sm-12 clearfix">
                            <div class="bill-to">
                                <p> Shipping Information</p>
                                <div class="form-one">
                                    <form action="{{route('CreateNewOrder')}}" method="post">
                                        {{csrf_field()}}
                                        <input type="text" name="email" placeholder="Email" required>
                                        <input type="text" name="first_name" placeholder="First Name" required>
                                        <input type="text" name="last_name" placeholder="Last Name" required>
                                        <input type="text" name="address" placeholder="Address" required>
                                        <input type="text" name="postal_code" placeholder="Zip / Postal Code" required>
                                        <label>
                                            <select>
                                                <option>Country</option>
                                                <option>United States</option>
                                                <option>Canada</option>
                                            </select>
                                        </label>
                                        <label>
                                            <select>
                                                <option>State / Province</option>
                                                <option>Manitoba</option>
                                                <option>Canada</option>
                                            </select>
                                        </label>
                                        <input type="text" name="phone" placeholder="Phone" required>

                                        <button class="btn btn-default check_out" type="submit" name="submit">Proceed To
                                            Payment
                                        </button>

                                    </form>
                                </div>
                                <div class="form-two">

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
        </div>
{{--        @else--}}
{{--            <div class="alert alert-danger" role="alert">--}}
{{--                <strong>Please!</strong> <a href="{{route('login') }}">Log in</a> in order to create an order--}}
{{--            </div>--}}
{{--        @endif--}}
    </section> <!--/#cart_items-->
    <section id="do_action">
        <div class="container">
            <div class="heading">
{{--                <h3>What would you like to do next?</h3>--}}
{{--                <p>Choose if you have a discount code or reward points you want to use or would like to estimate your--}}
{{--                    delivery cost.</p>--}}
            </div>
        </div>
    </section>
@endsection




