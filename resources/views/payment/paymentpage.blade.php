@extends('layouts.index')
@section('center')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
            <div class="shopper-information">
                <div class="row">
                    <div class="col-sm-12 clearfix">
                        <div class="bill-to">
                            <p> Shipping/Bill To</p>
                            <div class="form-one">
                                <div class="total_area">
                                    <ul class="list-group">
                                        <li>Name
                                            <span>{{$payment_info['first_name']." ".$payment_info['last_name']}}</span>
                                        </li>
                                        <li>Address
                                            <span>{{$payment_info['address'].", ".$payment_info['postal_code']}}</span>
                                        </li>
                                        <li>E-mail
                                            <span>{{$payment_info['email']}}</span>
                                        </li>
                                        <li>Phone #
                                            <span>{{$payment_info['phone']}}</span>
                                        </li>
                                        <li>Payment Status
                                            @if($payment_info['status'] == 'on_hold')
                                                <span>Awaiting payment</span>
                                            @endif
                                        </li>
                                        <li>Shipping Cost <span>Free</span></li>
                                        <li>Total <span>{{$payment_info['price']}}</span></li>
                                    </ul>
{{--                                        <a class="btn btn-default update" href="{{route('CheckoutProducts')}}">Update Info</a>--}}
{{--                                        <a class="btn btn-default check_out" id="paypal-button" ></a>--}}

                                    <div id="paypal-button-container"></div>


                                </div>
                            </div>
                            <div class="form-two">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section> <!--/#payment-->

    </section> <!--/#cart_items-->
    <section id="do_action">
        <div class="container">
            <div class="heading">
                {{--                <h3>What would you like to do next?</h3>--}}
                {{--                <p>Choose if you have a discount code or reward points you want to use or would like to estimate your--}}
                {{--                    delivery cost.</p>--}}
            </div>
        </div>
    </section>
@endsection

<script
    src="https://www.paypal.com/sdk/js?client-id=AfCm5GORr3xIbrK4U1hf1kltlC1ENRofyiOa44kb_umpam6aAOh0D_7f6uLZS5hQQA_RSouUKRHb7YUr&currency=CAD"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
</script>
<script>
    paypal.Buttons({
        createOrder: function(data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '{{$payment_info['price']}}'
                    }
                }]
            });
        },

        onApprove: function(data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.capture().then(function(details) {
                // This function shows a transaction success message to your buyer.
                alert('Transaction completed by ' + details.payer.name.given_name);
                window.location = './paymentreceipt/' + data.orderID + '/' + data.payerID;
            });
        }
    }).render('#paypal-button-container');
    //This function displays Smart Payment Buttons on your web page.
</script>


