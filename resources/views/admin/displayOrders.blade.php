@extends('layouts.admin')

@section('body')

    <h1 class="page-header">Orders</h1>
    @if (session('orderDeletionStatus'))
        <div class="alert alert-danger"> {{session('orderDeletionStatus')}}</div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped table-sm">
            <thead>
            <tr>
                <th>Order ID</th>
                <th>User ID</th>
                <th>Order Date</th>
                <th>Delivery Date</th>
                <th>Status</th>
                <th>Price</th>
            </tr>
            </thead>
            <tbody>

            @foreach($orders as $order)
                <tr>
                    <td>{{$order->order_id}}</td>
                    <td>{{$order->user_id}}</td>
                    <td>{{$order->date}}</td>
                    <td>{{$order->del_date}}</td>
                    <td>{{$order->status}}</td>
                    <td>{{$order->price}}</td>
                    <td><a href="{{ route('AdminDeleteOrder',['id' => $order->order_id ])}}"
                           onclick="return confirm('Are you sure you want to delete this order?')"
                           class="btn btn-warning">Remove</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{$orders->links()}}

    </div>

@endsection
