<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('order_id');
            $table->date("date");
            $table->String("status");
            $table->date("del_date");
            $table->float("price",8,2);
            $table->String("first_name");
            $table->String("last_name");
            $table->String("email");
            $table->String("address");
            $table->integer("phone");
            $table->String("postal_code");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
