<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Faker\Factory as Faker;
use PhpParser\Node\Expr\Cast\Double;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $faker = Faker::create();
        DB::table('products')->insert([
            'name' => $faker->word(),
            'description' => $faker->sentence($nbWords = 6, $variableNbWords = true),
            'image' => 'no_image.png',
            'price' => rand(1000, 10000)/100,
            'type' => 'none',
        ]);
        print('entry created');
    }
}
