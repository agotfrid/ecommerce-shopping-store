<?php

use App\Http\Controllers\Admin\AdminProductsController;
use App\Http\Controllers\Payment\PaymentsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ProductsController::class, 'index'])->name('allProducts');

// all products
Route::get('products', [ProductsController::class, 'index'])->name('allProducts');

// men products
Route::get('products/men', [ProductsController::class, 'menProducts'])->name('MenProducts');

// women products
Route::get('products/women', [ProductsController::class, 'womenProducts'])->name('WomenProducts');

//search
Route::get('search', [ProductsController::class, 'search'])->name('SearchProducts');

// add to cart
Route::get('product/addToCart/{id}', [ProductsController::class, 'addProductToCart'])->name('AddProductToCart');

// show cart items
Route::get('cart', [ProductsController::class, 'showCart'])->name('cartproducts');

// add quantity in cart
Route::get('product/increaseQuantity/{id}', [ProductsController::class, 'increaseCartQuantity'])->name('IncreaseCartQuantity');
// decrease quantity in cart
Route::get('product/reduceQuantity/{id}', [ProductsController::class, 'reduceCartQuantity'])->name('ReduceCartQuantity');
// delete item from cart
Route::get('product/deleteItemFromCart/{id}', [ProductsController::class, 'deleteItemFromCart'])->name('DeleteItemFromCart');

// process checkout page
Route::post('product/create-new-order', [ProductsController::class, 'createNewOrder'])->name('CreateNewOrder');

// checkout products to capture user info
Route::get('product/checkout-products', [ProductsController::class, 'checkoutProducts'])->name('CheckoutProducts');

// Payment page
Route::get('payment/payment-page', [PaymentsController::class, 'showPaymentPage'])->name('ShowPaymentPage');

// Payment receipt page
Route::get('payment/paymentreceipt/{paymentID}/{payerID}', [PaymentsController::class, 'showPaymentReceipt'])->name('ShowPaymentReceipt');

// User authentication
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Restrict access for admin panel only to admin users
Route::group(['middleware' => ['restrictToAdmin']], function(){
    // Admin panel
    Route::get('admin/products', [AdminProductsController::class, 'index'])->name('AdminDisplayProducts');
    Route::get('admin/orders', [AdminProductsController::class, 'ordersPanel'])->name('OrdersPanel');
    Route::get('admin/edit-product-form/{id}', [AdminProductsController::class, 'editProductForm'])->name('AdminEditProductForm');
    Route::get('admin/edit-product-image-form/{id}', [AdminProductsController::class, 'editProductImageForm'])->name('AdminEditProductImageForm');

// update product image
    Route::post('admin/update-product-image/{id}', [AdminProductsController::class, 'updateProductImage'])->name('AdminUpdateProductImage');

//update product data
    Route::post('admin/update-product/{id}', [AdminProductsController::class, 'updateProduct'])->name('AdminUpdateProduct');

// create new product
    Route::get('admin/add-product-form', [AdminProductsController::class, 'addProductForm'])->name('AdminAddProductForm');

//send new product data to database
    Route::post('admin/send-add-data-form', [AdminProductsController::class, 'sendAddProductForm'])->name('AdminSendAddProductForm');

// admin delete product
    Route::get('admin/delete-product/{id}', [AdminProductsController::class, 'deleteProduct'])->name('AdminDeleteProduct');

    // admin delete order
    Route::get('admin/delete-order/{id}', [AdminProductsController::class, 'deleteOrder'])->name('AdminDeleteOrder');
});


// Add to cart using ajax post request
    Route::post('products/addToCartAjaxPost', [ProductsController::class, 'addToCartAjaxPost'])->name('AddToCartAjaxPost');
//    Route::get('products/addToCartAjaxGet/{id}', [ProductsController::class, 'addToCartAjaxGet'])->name('AddToCartAjaxGet');
