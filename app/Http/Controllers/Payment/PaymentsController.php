<?php

namespace App\Http\Controllers\Payment;
use App\Http\Controllers\Controller;
use App\Mail\OrderCreatedEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PaymentsController extends Controller{

    public function showPaymentPage(){
        $payment_info = Session::get('payment_info'); // accessed from session

        // if user has not paid yet
        if($payment_info['status'] == 'on_hold'){
            return view('payment.paymentpage', ['payment_info'=>$payment_info]);
        }
        else{
            return redirect()->route('allProducts');
        }
    }

    public function showPaymentReceipt($paymentID, $payerID){
        if(!empty($paymentID) && !empty($payerID)){
//          will return json if approved, and will return else otherwise
            $this->validatePayment($paymentID, $payerID);
            $this->storePaymentInfo($paymentID, $payerID);
            $payment_receipt = Session::get('payment_info');
            $payment_receipt['paypal_payment_id'] = $paymentID;
            $payment_receipt['paypal_payer_id'] = $payerID;

            //send email to customer


            Session::forget('payment_info');
            Session::forget('cart');
            return view('payment.paymentreceipt', ['payment_receipt' => $payment_receipt]);
        }
        else{
            return redirect()->route('allProducts')->with('msg', 'Error! The payment did not go through!');
        }
    }

    private function sendEmail(){
        $user = Auth::user();
        $cart = Session::get('cart');

        if($cart != null && $user != null){
            Mail::to($user)->send(new OrderCreatedEmail($cart));
        }
    }


    /**
     * Stores the payment info that goes through paypal
     * @param $paymentID = paypal payment ID
     * @param $payerID = paypal payer ID
     */
    private function storePaymentInfo($paymentID, $payerID)
    {
        $payment_info = Session::get('payment_info');
        $order_id = $payment_info['order_id']; // non paypal order id
        $status = $payment_info['status'];


        if ($status == 'on_hold') {
            //create (issue) a new payment row in payments table
            $date = date('Y-m-d H:i:s');
            $newPaymentArray = array("order_id" => $order_id, "date" => $date, "amount" => $payment_info['price'],
                "paypal_payment_id" => $paymentID, "paypal_payer_id" => $payerID);
            $created_order = DB::table("payments")->insert($newPaymentArray);

            //update payment status in orders table to "paid"
            DB::table('orders')->where('order_id', $order_id)->update(['status' => 'paid']);
        }
    }

    /**
     * Calls to paypal api to verify transaction, will not work on localhost server
     * @param $paymentID
     * @param $payerID
     */
    public function validatePayment($paymentID, $payerID){
        $paypalEnv = 'sandbox';
        $paypalURL = 'https://api.sandbox.paypal.com/v1/';
        $paypalClientID = 'AfCm5GORr3xIbrK4U1hf1kltlC1ENRofyiOa44kb_umpam6aAOh0D_7f6uLZS5hQQA_RSouUKRHb7YUr';
        $paypalSecret = 'EA1O9y4K-qv0QM1O-Cw9J-xXcZ_v7XMmy5G4C1uXRTSOGoYkM4fddV9B1P88eR_VbLxbSmCha9eudRh-';

        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $paypalURL.'oauth2/token');
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, $paypalClientID.':'.$paypalSecret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');
        $response = curl_exec($ch);
        curl_close($ch);

        if(empty($response)){
            return false;
        }
        else{
            $jsonData = json_decode($response);
            $curl = curl_init($paypalURL.'payments/payment'.$paymentID);
            curl_setopt($curl, CURLOPT_POST, FALSE);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                'Authorization: bearer '.$jsonData->access_token,
                'Accept: application/json',
                'Content-Type: application/xml'
            ));
            $response = curl_exec($curl);
            curl_close($curl);

            //Transaction data
            return json_decode($response);
        }


    }

}
