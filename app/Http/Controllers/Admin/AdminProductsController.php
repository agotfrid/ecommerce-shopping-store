<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AdminProductsController extends Controller
{
    // Displays all products
    public function index()
    {
        $products = Product::paginate(10);
        return view('admin.displayProducts', ['products' => $products]);
    }

    public function ordersPanel()
    {
        $orders = DB::table('orders')->paginate(10);
        return view('admin.displayOrders', ['orders' => $orders]);
    }

    // edit product
    public function deleteOrder(Request $request, $id)
    {
        try {
            $order = DB::table('orders')->where('order_id', $id);
            if ($order->exists()) {
                $deleted = $order->delete();
                if ($deleted) {
                    return redirect()->back()->with('orderDeletionStatus', 'Order ' . $id . ' was deleted');
                } else {
                    return redirect()->back()->with('orderDeletionStatus', 'Order ' . $id . ' was deleted');
                }
            }
        } catch (Exception $e) {
            echo 'Caught exception: ', $e->getMessage(), "\n";
        }
        return redirect()->back();
    }

    // add new product
    public function addProductForm()
    {
        return view('admin.addProductForm');
    }

    public function sendAddProductForm(Request $request)
    {

        $name = $request->input('name');
        $description = $request->input('description');
        $type = $request->input('type');
        $price = $request->input('price');
        Validator::make($request->all(), ['image' => 'required|image|mimes:jpg,png,jpeg|max:5000'])->validate();
        $extension = $request->file('image')->getClientOriginalExtension();
        $stringImgReFormat = str_replace(" ", "", $request->input('name'));
        $imageName = $stringImgReFormat . "." . $extension;
        $request->image->storeAs('public/product_images/', $imageName);

        // Alternative way to store image
//        $imageEncoded = File::get($request->image); // encode it first
//        Storage::disk('local')->put('product_images/'.$imageName, $imageEncoded);

        $newProductArray = array('name' => $name, 'description' => $description, 'image' => $imageName, 'type' => $type, 'price' => $price);
        $created = DB::table('products')->insert($newProductArray);
        if ($created) {
            return redirect()->route('AdminDisplayProducts');
        } else {
            return "Product was not created";
        }

    }

    // edit product
    public function editProductForm($id)
    {
        $products = Product::find($id);
        return view('admin.editProductForm', ['product' => $products]);
    }

    // edit product
    public function deleteProduct($id)
    {
        $product = Product::find($id);
        $exists = Storage::disk('local')->exists('public/product_images/' . $product->image);

        //delete old image
        if ($exists) {
            Storage::delete('public/product_images/' . $product->image);
        }

        Product::destroy($id);

        return redirect()->route('AdminDisplayProducts');
    }

    // edit product image
    public function editProductImageForm($id)
    {
        $products = Product::find($id);
        return view('admin.editProductImageForm', ['product' => $products]);
    }

    public function updateProductImage(Request $request, $id)
    {
        try {
            Validator::make($request->all(), ['image' => 'required|image|mimes:jpg,png,jpeg|max:5000'])->validate();

            if ($request->hasFile('image')) {
                $product = Product::find($id);
                $exists = Storage::disk('local')->exists('public/product_images/' . $product->image);

                //delete old image
                if ($exists) {
                    Storage::delete('public/product_images/' . $product->image);
                }

                // read extension
//                $extension = $request->file('image')->getClientOriginalExtension();

//                upload new image
                $request->image->storeAs('public/product_images/', $product->image);

                $arrayToUpdate = array('image' => $product->image);
                DB::table('products')->where('id', $id)->update($arrayToUpdate);

                return redirect()->route('AdminDisplayProducts');
            } else {
                return "No image was selected";
            }
        } catch (ValidationException $e) {
            return report($e);
        }
    }

    public function updateProduct(Request $request, $id)
    {
        $name = $request->input('name');
        $description = $request->input('description');
        $type = $request->input('type');
        $price = $request->input('price');

        $arrayToUpdate = array('name' => $name, 'description' => $description, 'type' => $type, 'price' => $price);
        DB::table('products')->where('id', $id)->update($arrayToUpdate);

        return redirect()->route('adminDisplayProducts');
    }
}
