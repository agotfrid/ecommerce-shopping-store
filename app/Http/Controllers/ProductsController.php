<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ProductsController extends Controller
{
    //
    public function index()
    {
        $products = Product::paginate(10);
        return view("allproducts", compact("products"));
    }

    function menProducts()
    {
        $products = Product::where('type', 'Men')->paginate(10);
        return view("allproducts", compact("products"));

    }

    function womenProducts()
    {
        $products = Product::where('type', 'Women')->paginate(10);
        return view("allproducts", compact("products"));
    }

    function search(Request $request)
    {
        $searchProducts = $request->get('searchProducts');
        $products = Product::where('name', 'Like', '%' . $searchProducts . '%')->paginate(10);
        return view("allproducts", compact("products"));
    }

    public function addProductToCart(Request $request, $id)
    {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);

        $product = Product::find($id);
        $cart->addItem($id, $product);
        $request->session()->put('cart', $cart);
//        dump($cart)
        return redirect()->route('allProducts');
    }

    function deleteItemFromCart(Request $request, $id)
    {
        $cart = $request->session()->get('cart');
        if (array_key_exists($id, $cart->items)) {
            unset($cart->items[$id]);
            // recalculate quantities

        }

        $prevCart = $request->session()->get('cart');
        $updatedCart = new Cart($prevCart);
        $updatedCart->updatePriceAndQuantity();

        $request->session()->put('cart', $updatedCart);
        return redirect()->route('cartproducts');
    }

    function increaseCartQuantity(Request $request, $id)
    {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);

        $product = Product::find($id);
        $cart->addItem($id, $product);
        $request->session()->put('cart', $cart);
        $cart->updatePriceAndQuantity();

        return redirect()->route('cartproducts');
    }

    function reduceCartQuantity(Request $request, $id)
    {
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);

        if ($prevCart->items[$id]['quantity'] == 1) {
            return $this->deleteItemFromCart($request, $id);
        } else if ($cart->items[$id]['quantity'] > 1) {
            $product = Product::find($id);
            $cart->items[$id]['quantity'] -= 1;
            $cart->items[$id]['totalItemPrice'] = $cart->items[$id]['quantity'] * $product['price'];
            $cart->updatePriceAndQuantity();

            $request->session()->put('cart', $cart);
        }

        return redirect()->route('cartproducts');
    }

    public function createNewOrder(Request $request)
    {
        $cart = Session::get('cart'); // get data from cart

        $first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
        $email = $request->input('email');
        $address = $request->input('address');
        $phone = $request->input('phone');
        $postal_code = $request->input('postal_code');

        // if user is not a guest userID will have number other than 0
        $userID = (Auth::check() ? Auth::id() : 0);

        if ($cart) { // if not empty
            $date = date('Y-m-d H:i:s');
            $newOrderArray = array('user_id' => $userID, 'date' => $date, 'status' => 'on_hold', 'del_date' => $date,
                'price' => $cart->totalPrice, 'first_name' => $first_name,
                'last_name' => $last_name, 'email' => $email, 'address' => $address, 'phone' => $phone, 'postal_code' => $postal_code
            );
            $created_order = DB::table('orders')->insert($newOrderArray);
            $order_id = DB::getPdo()->lastInsertId();

            foreach ($cart->items as $cart_item) {
                $item_id = $cart_item['data']['id'];
                $item_name = $cart_item['data']['name'];
                $item_price = $cart_item['data']['price'];
                $newItemsInOrder = array('item_id' => $item_id, 'order_id' => $order_id, 'item_name' => $item_name, 'item_price' => $item_price);
                $created_order_items = DB::table('order_items')->insert($newItemsInOrder);
            }

            $payment_info = $newOrderArray;
            $payment_info['order_id'] = $order_id; // assign order id so it is accessible in the session
            $request->session()->put('payment_info', $payment_info);

            return redirect()->route('ShowPaymentPage');

        } else {
            return redirect()->route('allProducts')->with('failure', 'There was an error placing the order');
        }
    }

    public function checkoutProducts()
    {
        return view('checkoutproducts');
    }

    public function showCart()
    {
        $cart = Session::get('cart');

        //cart is not empty
        if ($cart != null) {
            return view('cartproducts', ['cartItems' => $cart]);
        } else { // cart is empty
            return redirect()->route('allProducts');
        }
    }

    public function addToCartAjaxPost(Request $request){
        $id = $request->input('id'); // gets the id from request
        $prevCart = $request->session()->get('cart');
        $cart = new Cart($prevCart);

        $product = Product::find($id);
        $cart->addItem($id, $product);
        $request->session()->put('cart', $cart);

        return response()->json(['totalQuantity' => $cart->totalQuantity]);
    }

//    function addToCartAjaxGet(Request $request, $id){
//        $prevCart = $request->session()->get('cart');
//        $cart = new Cart($prevCart);
//
//        $product = Product::find($id);
//        $cart->addItem($id, $product);
//        $request->session()->put('cart', $cart);
//
//        return response()->json(['cart', 'Item has been successfully added to cart']);
//    }


}
