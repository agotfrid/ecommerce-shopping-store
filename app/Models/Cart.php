<?php


namespace App\Models;


class Cart
{
    public $items; //['id' => ['quantity' => 1, 'totalItemPrice' => 55.65, 'data' => ]]
    public $totalQuantity;
    public $totalPrice;

    /**
     * Cart constructor.
     * @param $prevCart
     */
    public function __construct($prevCart)
    {
        if($prevCart != null){
            $this->items = $prevCart->items;
            $this->totalQuantity = $prevCart->totalQuantity;
            $this->totalPrice = $prevCart->totalPrice;
        }
        else{
            $this->items = [];
            $this->totalQuantity = 0;
            $this->totalPrice = 0;
        }
    }

    public function addItem($id, $product){
        $price = str_replace('$', '', $product->price);
        if(array_key_exists($id, $this->items)){ // The item already exists
            $productToAdd = $this->items[$id];
            $productToAdd['quantity']++;
            $productToAdd['totalItemPrice'] = $productToAdd['quantity'] * $price;
        }
        else { // First time to add this product to cart
            $productToAdd = ['quantity' => 1, 'totalItemPrice' => $price, 'data' => $product];
        }

        $this->items[$id] = $productToAdd;
        $this->totalQuantity++;
        $this->totalPrice += $price;
    }

    public function updatePriceAndQuantity(){
        $totalPrice = 0;
        $totalQuantity = 0;

        foreach ($this->items as $item){
            $totalQuantity += $item['quantity'];
            $totalPrice += $item['totalItemPrice'];
        }

        $this->totalPrice = $totalPrice;
        $this->totalQuantity = $totalQuantity;
    }
}
